# SDK Reader 

### Notes 
> This was not made for tablet! 

 * Minimum Android is Lollipop 5.0 

### Screenshot 
![title](app_screenshot.PNG)

:checkered_flag: [Download APK](https://gitlab.com/android-development3/sdk-reader/-/raw/master/app/release/app-release.apk)

**Development By,** <br>
***:bd: Farhan Sadik***

### Sources 
https://developer.android.com/reference/android/os/Build.VERSION_CODES <br>
https://stackoverflow.com/questions/3423754/retrieving-android-api-version-programmatically