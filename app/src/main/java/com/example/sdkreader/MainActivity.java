package com.example.sdkreader;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    String ModelNumber, Brand, AndroidVersion, SecurityPatch;
    int API;
    TextView text1, text2, text3, text4;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AndroidVersion = android.os.Build.VERSION.RELEASE;
        //APILevel = android.os.Build.VERSION.SDK;
        API = android.os.Build.VERSION.SDK_INT; // for use as integer
        SecurityPatch = android.os.Build.VERSION.SECURITY_PATCH;

        imageView = findViewById(R.id.android_image);
        //imageView.setImageResource(R.drawable.apple_icon);

        // defining logo
        if (API == android.os.Build.VERSION_CODES.LOLLIPOP) {
            imageView.setImageResource(R.drawable.android_lollipop_logo);
        } else if (API == Build.VERSION_CODES.LOLLIPOP_MR1){
            imageView.setImageResource(R.drawable.android_lollipop_logo);
        } else if (API == Build.VERSION_CODES.M){
            imageView.setImageResource(R.drawable.android_marshmallow_logo);
        } else if (API == Build.VERSION_CODES.N){
            imageView.setImageResource(R.drawable.android_n_logo);
        } else if (API == Build.VERSION_CODES.N_MR1){
            imageView.setImageResource(R.drawable.android_n_logo);
        } else if (API == Build.VERSION_CODES.O){
            imageView.setImageResource(R.drawable.android_oreo_logo);
        } else if (API == Build.VERSION_CODES.O_MR1){
            imageView.setImageResource(R.drawable.android_oreo_logo);
        } else if (API == Build.VERSION_CODES.P){
            imageView.setImageResource(R.drawable.android_p_logo);
        } else if (API == Build.VERSION_CODES.Q){
            imageView.setImageResource(R.drawable.android_10_logo);
        } else if (API == Build.VERSION_CODES.R){
            imageView.setImageResource(R.drawable.android_11_logo);
        } else {
            imageView.setImageResource(R.drawable.android_default_logo);
        }

        text1 = (TextView) findViewById(R.id.deviceInfo_templete);
        text2 = (TextView) findViewById(R.id.deviceInfo);

        text1.setText("Android Version "
                + "\nAPI Level "
                + "\nRelease Date");

        text2.setText(AndroidVersion
                + "\n" + API
                + "\n" + SecurityPatch);

        ModelNumber = android.os.Build.MODEL;
        Brand = android.os.Build.BRAND;

        text3 = (TextView) findViewById(R.id.model_brand_info_templete);
        text4 = (TextView) findViewById(R.id.model_brand_info);

        text3.setText("Device Model " + "\nManufacturer ");
        text4.setText(ModelNumber + "\n" + Brand);

    }

    // menu on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    // on menu option
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.cardview_actionbar:
                startActivity(new Intent(getApplicationContext(), CardViewAPIListing.class));
                break;
            case R.id.about_application:
                startActivity(new Intent(getApplicationContext(), AboutApplication.class));
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
